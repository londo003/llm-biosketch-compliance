#!/usr/local/bin/python3
import argparse
from pathlib import Path
from biosketch_ai import BioSketch
from biosketch_validator import BioSketchValidator

def show_errors(archive):
    biosketch = BioSketch.from_archive(archive)
    biosketch_path = Path(biosketch.biosketch_file)
    validator = BioSketchValidator.for_granting_institution(biosketch.granting_institution)
    print(f'Biosketch {biosketch_path.name} version {BioSketch.version()} validated for {biosketch.granting_institution} by validator version {type(validator).version()}')
    print(f'{len(biosketch.errors)} Errors for {biosketch_path.name}:\n- '+"\n- ".join(biosketch.errors))

def show_latest(biosketch_path):
    biosketch_path = Path(biosketch_path)
    show_errors(BioSketch.archives(biosketch_path.name)[-1])

def show_archives(biosketch):
    biosketch_path = Path(biosketch)
    print(f'Avaliable archives for {biosketch_path.name}:\n'+"\n".join(BioSketch.archives(biosketch_path.name)))

if __name__ == "__main__":
    # Argument parser setup
    parser = argparse.ArgumentParser(description='Show Information for a Biosketch.')
    parser.add_argument('--biosketch', type=str, help='Path to biosketch')
    parser.add_argument('--latest', action='store_true', help='if set with --biosketch shows errors for the latest archive')
    parser.add_argument('--archives', action='store_true', help='if true, show archives for which errors can be shown. Must also set --biosketch.')
    parser.add_argument('--archive', type=str, help='archive for which to show errors, see a list of available archives for a biosketch using --archives.')
    args = parser.parse_args()
        
    if args.archive:
        show_errors(args.archive)
    elif args.biosketch:
        if args.archives:
            show_archives(args.biosketch)
        elif args.latest:
            show_latest(args.biosketch)
        else:
            parser.print_help()
    else:
        parser.print_help()
