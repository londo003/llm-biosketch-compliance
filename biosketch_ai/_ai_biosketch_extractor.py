from openai import OpenAI, AzureOpenAI
import os
import time

class AIBioSketchExtractor:
    def __init__(self, model, provider, usage_tracker=None):
        self.model = model
        self.provider = provider
        self.usage_tracker = usage_tracker
        self.random_seed = int(time.time())
        self._get_client()

    def _get_client(self):
        """
        Configures OpenAI API credentials based on the specified provider.
        """
        if self.provider == "azure":
            self.client = AzureOpenAI(
                azure_endpoint=os.getenv("AZURE_OPENAI_ENDPOINT"),
                api_key=os.getenv("AZURE_OPENAI_KEY"),
                api_version=os.getenv("AZURE_OPENAI_VERSION")
            )
        elif self.provider == "openai":
            self.client = OpenAI(
                api_key=os.getenv("OPENAI_API_KEY"),
                api_type="openai"
            )
        else:
            raise ValueError("Unsupported provider specified.")


    def extract(self, data, tool):
        """
        Calls the OpenAI API to summarize data provided in text or image form.
        """
        system_prompt = "You are a detail-oriented grants administrator. Extract details from biosketches. Text should be extracted verbatim. Be as precise as possible. If you can't find a value, return an empty value ''. Only respond in JSON."
        messages = [{"role": "system", "content": system_prompt}, {"role": "user", "content": data}]
        response_format = {"type": "json_object"}
        response = self.client.chat.completions.create(
            model=self.model,
            messages=messages,
            tools=[tool],
            tool_choice={"type": "function", "function": {"name": tool['function']['name']}},
            response_format=response_format,
            temperature=0.5,
            seed=self.random_seed
        )
        if self.usage_tracker:
            self.usage_tracker.track(
                tool['function']['name'],
                response.usage.prompt_tokens, 
                response.usage.completion_tokens)
        return response.choices[0].message.tool_calls[0].function.arguments

    @classmethod
    def version(cls):
        return 'v2.0.0'