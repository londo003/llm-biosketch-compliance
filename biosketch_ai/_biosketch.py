import pdfplumber
import re
import os
from pathlib import Path
from datetime import datetime, timezone
from typing import List, Optional
from collections import Counter
from pydantic import BaseModel, Field, field_validator

class Manuscript(BaseModel):
    manuscript_first_author: Optional[str] = Field(default=None, description="The first author of the manuscript.", validate_default=False)
    manuscript_title: Optional[str] = Field(default=None, description="The title of the manuscript.", validate_default=False)
    manuscript_journal: Optional[str] = Field(default=None, description="The journal the manuscript was published in.", validate_default=False)
    manuscript_year: Optional[datetime] = Field(default=None, description="The publication year of the manuscript.", validate_default=False)
    manuscript_pmcid: Optional[str] = Field(default=None, description="The PubMed Central ID (PMCID) associated with the manuscript. A string that must start with PMC eg. 'PMC1234567'. only include if you find the PMC string.", validate_default=False)
    
    @field_validator('manuscript_year', mode='before', check_fields=False)
    @classmethod
    def year_to_date(cls, v: datetime):
        """
            returns a utc datetime object from v
            - if already a datetime, return v
            - if it is stored in isoformat, create and return it
            - it will come in from a biosketch as YYYY, return as utc
        """
        ret_date = None
        if isinstance(v, datetime):
            ret_date = v.replace(tzinfo=timezone.utc)
        else:
            if v:
                try:
                    ret_date = datetime.fromisoformat(str(v)).replace(tzinfo=timezone.utc)
                except ValueError:
                    match = re.search(r'\b(\d{4})\b', str(v))
                    if match:
                        ret_date = datetime.strptime(match.group(1),"%Y").replace(tzinfo=timezone.utc)
        return ret_date

    @classmethod
    def required_attributes(cls):
        return ["manuscript_first_author", "manuscript_title", "manuscript_journal", "manuscript_year", "manuscript_pmcid"]

class ResearchProject(BaseModel):
    identifier: Optional[str] = Field(default=None, description="The institutional identifer for project.", validate_default=False)
    pi_name: Optional[str] = Field(default=None, description="The PI of the project.", validate_default=False)
    project_role: Optional[str] = Field(default=None, description="The role of the person in the project.", validate_default=False)
    start_date: Optional[datetime] = Field(default=None, description="The start date of project.", validate_default=False)
    end_date: Optional[datetime] = Field(default=None, description="The end date of project.", validate_default=False)
    title: Optional[str] = Field(default=None, description="The title of the project.", validate_default=False)

    @field_validator('start_date', 'end_date', mode='before', check_fields=False)
    @classmethod
    def year_to_date(cls, v: datetime):
        """
            returns a utc datetime object from v
            - if already a datetime, return v
            - if it is stored in isoformat, create and return it
            - it will come in from a biosketch as YYYY, return as utc
        """
        ret_date = None
        if isinstance(v, datetime):
            ret_date = v.replace(tzinfo=timezone.utc)
        else:
            if v:
                try:
                    ret_date = datetime.fromisoformat(str(v)).replace(tzinfo=timezone.utc)
                except ValueError:
                    match = re.search(r'\b(\d{4})\b', str(v))
                    if match:
                        ret_date = datetime.strptime(match.group(1),"%Y").replace(tzinfo=timezone.utc)
        return ret_date

    @classmethod
    def required_attributes(cls):
        return ["identifier", "pi_name", "project_role", "start_date", "end_date", "title"]

class ScientificContribution(BaseModel):
    contribution_summary: Optional[str] = Field(default=None, description="One sentence summary of contribution to science.", validate_default=False)
    contribution_description: Optional[str] = Field(default=None, description="Description of contribution to science.", validate_default=False)
    manuscripts: List[Manuscript] = Field(default=[], description="List of manuscripts citations associated with contribution to science.", validate_default=False)

    @classmethod
    def required_attributes(cls):
        return ["contribution_summary", "contribution_description", "manuscripts"]

class PersonalStatement(BaseModel):
    statement: Optional[str] = Field(default=None, description="The personal statement text.", validate_default=False)
    point_of_view: Optional[str] = Field(default=None, description="The point of view of the personal_statement, must be one of first, second, third.", validate_default=False)
    research_projects: List[ResearchProject] = Field(default=[], description="List of Ongoing and recently completed projects associated with the personal statement.", validate_default=False)
    manuscripts: List[Manuscript] = Field(default=[], description="List of manuscripts citations associated with the personal statement.", validate_default=False)

    @classmethod
    def required_attributes(cls):
        return ["statement", "point_of_view","research_projects", "manuscripts"]

class EducationalInstitution(BaseModel):
    name: Optional[str] = Field(default=None, description="The name of the institution", validate_default=False)
    location: Optional[str] = Field(default=None, description="The location of the institution", validate_default=False)

    @classmethod
    def required_attributes(cls):
        return ["name", "location"]

class Education(BaseModel):
    institution: EducationalInstitution = Field(default=None, description = "The institution where the education was received.", validate_default=False)
    degree: Optional[str] = Field(default=None, description="The degree received.", validate_default=False)
    completion_date: Optional[datetime] = Field(default=None, description="The completion date of the degree.", validate_default=False)
    field_of_study: Optional[str] = Field(default=None, description="The field of study.", validate_default=False)

    @field_validator('completion_date', mode='before', check_fields=False)
    @classmethod
    def mon_year_to_date(cls, v: datetime):
        """
            returns a utc datetime object from v
            - if already a datetime, return v
            - if it is stored in isoformat, create and return it
            - it will come in from a biosketch as MM/YYYY, return as utc
        """
        ret_date = None
        if isinstance(v, datetime):
            ret_date = v.replace(tzinfo=timezone.utc)
        else:
            if v:
                try:
                    ret_date = datetime.fromisoformat(str(v)).replace(tzinfo=timezone.utc)
                except ValueError:
                    match = re.search(r'(\d{2}\/\d{4})', str(v))
                    if match:
                        ret_date = datetime.strptime(match.group(1),"%m/%Y").replace(tzinfo=timezone.utc)
        return ret_date

    @classmethod
    def required_attributes(cls):
        return ["institution", "degree", "completion_date", "field_of_study"]

class Honor(BaseModel):
    honor: Optional[str] = Field(default=None, description="The honor received.", validate_default=False)
    start_date: Optional[datetime] = Field(default=None, description="The start date of the honor.", validate_default=False)
    end_date: Optional[datetime] = Field(default=None, description="The end date of the honor.", validate_default=False)

    @field_validator('start_date', 'end_date', mode='before', check_fields=False)
    @classmethod
    def year_to_date(cls, v: datetime):
        """
            returns a utc datetime object from v
            - if already a datetime, return v
            - if it is stored in isoformat, create and return it
            - it will come in from a biosketch as YYYY, return as utc
        """
        ret_date = None
        if isinstance(v, datetime):
            ret_date = v.replace(tzinfo=timezone.utc)
        else:
            if v:
                try:
                    ret_date = datetime.fromisoformat(str(v)).replace(tzinfo=timezone.utc)
                except ValueError:
                    match = re.search(r'\b(\d{4})\b', str(v))
                    if match:
                        ret_date = datetime.strptime(match.group(1),"%Y").replace(tzinfo=timezone.utc)
        return ret_date

    @classmethod
    def required_attributes(cls):
        return ["start_date", "end_date", "honor"]

class ScientificAppointment(BaseModel):
    appointment: Optional[str] = Field(default=None, description="The scientific appointment.", validate_default=False)
    start_date: Optional[datetime] = Field(default=None, description="The start date of the appointment.", validate_default=False)
    end_date: Optional[datetime] = Field(default=None, description="The end date of the appointment.", validate_default=False)

    @field_validator('start_date', 'end_date', mode='before', check_fields=False)
    @classmethod
    def year_to_date(cls, v: datetime):
        """
            returns a utc datetime object from v
            - if already a datetime, return v
            - if it is stored in isoformat, create and return it
            - it will come in from a biosketch as YYYY, return as utc
        """
        ret_date = None
        if isinstance(v, datetime):
            ret_date = v.replace(tzinfo=timezone.utc)
        else:
            if v:
                try:
                    ret_date = datetime.fromisoformat(str(v)).replace(tzinfo=timezone.utc)
                except ValueError:
                    match = re.search(r'\b(\d{4})\b', str(v))
                    if match:
                        ret_date = datetime.strptime(match.group(1),"%Y").replace(tzinfo=timezone.utc)
        return ret_date

    @classmethod
    def required_attributes(cls):
        return ["start_date", "end_date", "appointment"]

class Position(BaseModel):
    position: Optional[str] = Field(default=None, description="The position held.", validate_default=False)
    start_date: Optional[datetime] = Field(default=None, description="The start date of the position.", validate_default=False)
    end_date: Optional[datetime] = Field(default=None, description="The end date of the position.", validate_default=False)

    @field_validator('start_date', 'end_date', mode='before', check_fields=False)
    @classmethod
    def year_to_date(cls, v: datetime):
        """
            returns a utc datetime object from v
            - if already a datetime, return v
            - if it is stored in isoformat, create and return it
            - it will come in from a biosketch as YYYY, return as utc
        """
        ret_date = None
        if isinstance(v, datetime):
            ret_date = v.replace(tzinfo=timezone.utc)
        else:
            if v:
                try:
                    ret_date = datetime.fromisoformat(str(v)).replace(tzinfo=timezone.utc)
                except ValueError:
                    match = re.search(r'\b(\d{4})\b', str(v))
                    if match:
                        ret_date = datetime.strptime(match.group(1),"%Y").replace(tzinfo=timezone.utc)
        return ret_date

    @classmethod
    def required_attributes(cls):
        return ["start_date", "end_date", "position"]

class BioSketch(BaseModel):
    granting_institution: Optional[str] = Field(default=None, description="granting institution for which the biosketch is to be submitted.", validate_default=False)
    biosketch_file: Optional[str] = Field(default=None, description = "The path to the biosketch PDF", repr=False, validate_default=False)
    errors: List[str] = Field(default=[], validate_default=False)
    validator_version: Optional[str] = Field(default=None, description="version of the BioSketchValidator used to validate the biosketch.", validate_default=False)
    min_x1: Optional[float] = Field(default=None, description="The minimum distance between the right side of any character and the left side of the page.", validate_default=False)
    max_x0:  Optional[float] = Field(default=None, description="The maximum distance between the left side of any character and the left side of the page.", validate_default=False)
    left_margin: Optional[float] = Field(default=None, description="The left margin of the biosketch document.")
    right_margin: Optional[float] = Field(default=None, description="The right margin of the biosketch document.")
    most_common_font: Optional[str] = Field(default=None, description="The most common font in the biosketch document.", validate_default=False)
    most_common_size: Optional[float] = Field(default=None, description="The most common font size in the biosketch document.", validate_default=False)
    colored_segments: List[dict] = Field(default=[], description="A List of non url characters with color in the biosketch document.", validate_default=False)
    unacceptable_urls: List[str] = Field(default=[], description="A list of urls that are not to ncbi.nlm.nih.gov.", validate_default=False)
    page_count: int = Field(default=0, description="The number of pages parsed from biosketch PDF", validate_default=False)
    full_text: Optional[str] = Field(default=None, repr=False, validate_default=False)
    approved_through: Optional[datetime] = Field(default=None, description="The approval date through which the document is valid.", validate_default=False)
    bibliography_link: Optional[str] = Field(default=None, description="Link to the complete list of published work in MyBibliography.", validate_default=False)
    era_commons_username: Optional[str] = Field(default=None, description="The eRA Commons username.", validate_default=False)
    name: Optional[str] = Field(default=None, description="The full name of the person.", validate_default=False)
    position_title: Optional[str] = Field(default=None, description="The position title.", validate_default=False)
    omb_no: List[str] = Field(default=[], description="List of OMB numbers.", validate_default=False)
    revision_date: Optional[datetime] = Field(default=None, description="The revision date of the document.", validate_default=False)
    personal_statement: PersonalStatement = Field(default=PersonalStatement(), description="The personal statement.", validate_default=False)
    contributions_to_science: List[ScientificContribution] = Field(default=[], description="List of contributions to sciences from section C of biosketech.", validate_default=False)
    education: List[Education] = Field(default=[], description="Educational background including institution, degree, completion date, and field of study.", validate_default=False)
    honors: List[Honor] = Field(default=[], description="List of honors received with dates.", validate_default=False)
    positions: List[Position] = Field(default=[], description="List of positions held with dates.", validate_default=False)
    scientific_appointments: List[ScientificAppointment] = Field(default=[], description="List of scientific appointments with dates.")

    @field_validator('approved_through', mode='before', check_fields=False)
    @classmethod
    def mon_day_year_to_date(cls, v: datetime):
        """
            returns a datetime object from v
            - if already a datetime, return v
            - if it is stored in isoformat, create and return it
            - it will come in from a biosketch as MM/DD/YYYY
        """
        ret_date = None
        if isinstance(v, datetime):
            ret_date = v.replace(tzinfo=timezone.utc)
        else:
            if v:
                try:
                    ret_date = datetime.fromisoformat(str(v)).replace(tzinfo=timezone.utc)
                except ValueError:
                    match = re.search(r'(\d{2}\/\d{2}\/\d{4})', str(v))
                    if match:
                        ret_date = datetime.strptime(match.group(1),"%m/%d/%Y").replace(tzinfo=timezone.utc)
        return ret_date

    @field_validator('revision_date', mode='before', check_fields=False)
    @classmethod
    def mon_year_to_date(cls, v: datetime):
        """
            returns a datetime object from v
            - if already a datetime, return v
            - if it is stored in isoformat, create and return it
            - it will come in from a biosketch as MM/YYYY
        """
        ret_date = None
        if isinstance(v, datetime):
            ret_date = v.replace(tzinfo=timezone.utc)
        else:
            if v:
                try:
                    ret_date = datetime.fromisoformat(str(v)).replace(tzinfo=timezone.utc)
                except ValueError:
                    match = re.search(r'(\d{2}\/\d{4})', str(v))
                    if match:
                        ret_date = datetime.strptime(match.group(1),"%m/%Y").replace(tzinfo=timezone.utc)
        return ret_date

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._extract_text_from_pdf()
        self._extract_urls()

    @classmethod
    def version(cls):
        return 'v2.1.0'

    @classmethod
    def archive_dir(cls):
        return f'archive/{cls.version()}'

    def _extract_text_from_pdf(self):
        """
        Extracts text and page count from a PDF file.
        """
        if self.biosketch_file:
            self.min_x1 = float('inf')
            self.max_x0 = float('-inf')
            font_counter = Counter()
            size_counter = Counter()
            url_pattern = re.compile(r'https?://[^\s/$.?#].[^\s]*', re.IGNORECASE)
            with pdfplumber.open(self.biosketch_file) as pdf:
                all_urls = set()

                for page in pdf.pages:
                    page_text = page.extract_text(x_tolerance=1, y_tolerance=3)
                    if page_text:
                        if self.full_text:
                            self.full_text += page_text
                        else:
                            self.full_text = page_text
                        all_urls.update(url_pattern.findall(page_text))

                    current_segment = ''
                    last_color = None

                    for char in page.chars:
                        font_counter[char['fontname']] += 1
                        size_counter[char['size']] += 1
                        if char['x1'] < self.min_x1:
                            self.min_x1 = char['x1']
                        if char['x0'] > self.max_x0:
                            self.max_x0 = char['x0']

                        if char.get('ncs') == 'DeviceRGB':
                            color = char.get('stroking_color') or char.get('non_stroking_color')
                            if color:
                                if color == last_color:
                                    current_segment += char['text']
                                else:
                                    if current_segment and last_color:
                                        is_url = any(current_segment.strip() in url for url in all_urls)
                                        if not is_url:
                                            self.colored_segments.append({'segment': current_segment.strip(), 'color': last_color})
                                    current_segment = char['text']
                                    last_color = color

                    # Append the last segment if any
                    if current_segment and last_color:
                        is_url = any(current_segment.strip() in url for url in all_urls)
                        if not is_url:
                            self.colored_segments.append({'segment': current_segment.strip(), 'color': last_color})

                self.page_count = len(pdf.pages)
                self.most_common_font = font_counter.most_common(1)[0][0] if font_counter else None
                self.most_common_size = size_counter.most_common(1)[0][0] if size_counter else None
                self.left_margin = self.min_x1/72
                self.right_margin = (612-self.max_x0)/72

    def _extract_urls(self):
        """
        Checks for unacceptable URLs in the provided text.
        The only acceptable URL is to ncbi.nlm.nih.gov.
        """
        # Regular expression to find URLs
        url_pattern = re.compile(r'https?://[^\s/$.?#].[^\s]*')

        # Find all URLs in the text
        urls = url_pattern.findall(self.full_text)

        # Check for unacceptable URLs
        self.unacceptable_urls = [url for url in urls if "ncbi.nlm.nih.gov" not in url]

    def archive(self):
        archive_file = f'{self.__class__.archive_dir()}/{os.path.basename(self.biosketch_file)}_{datetime.now().strftime("%Y_%b_%d_%H_%M_%S")}.json'
        with open(archive_file, 'w') as archive:
            archive.write(self.model_dump_json())

    def has_unacceptable_urls(self):
        return (len(self.unacceptable_urls) > 0)

    def contains_duke_medical_center_mention(self):
        duke_med_pattern =r"\bDuke\s+(?:University\s+)?Med(?:ical)?\.?\s+Ctr\b|\bDuke\s+(?:University\s+)?Medical\s+Center\b"
# Search for the pattern in the text
        duke_med_match = re.search(duke_med_pattern, self.full_text)
        if duke_med_match:
            return True
        else:
            return False

    # function to get part of the biosketch targetted to a tool
    def data_for_tool(self, tool):
        functions = {
            "extract_biographical_details": re.compile('^(.*?)A\\.\\sPersonal\\s*Statement', re.DOTALL),
            "extract_personal_statement": re.compile('^.*(A\\.\\sPersonal\\s*Statement.*?)B\\.\\sPositions', re.DOTALL),
            "extract_positions_appointments_honors": re.compile('^.*(B\\.\\sPositions.*?)C\\.\\sContribution', re.DOTALL),
            "extract_contributions_to_science": re.compile('^.*(C\\.\\sContribution.*[Ss]cience.*)$', re.DOTALL),
            "extract_bibliography_link": re.compile('^.*([Bb]+ibliography.*:.*?)$', re.DOTALL)
        }
        function = functions[tool['function']['name']]
        match = re.search(function, self.full_text)
        if match:
            return match.group(1)
        else:
            return None

    @classmethod
    def archives(cls, biosketch_file):
        unsorted_files = filter(os.path.isfile, [f'{cls.archive_dir()}/{f}' for f in os.listdir(cls.archive_dir()) if biosketch_file in f])
        return [f'{Path(f).name}' for f in sorted(unsorted_files, key=os.path.getmtime)]

    @classmethod
    def from_archive(cls, archive_file):
        relative_path = f'{cls.archive_dir()}/{archive_file}'
        if os.path.isfile(relative_path):
            with open(relative_path, 'r') as archive:
                json_data = archive.read()
            return cls.model_validate_json(json_data)
        else:
            raise ValueError(f'{archive_file} does not exist in {cls.archive_dir()}')

    # tools for LLM completions
    @classmethod
    def tools(cls):
        schema = cls.model_json_schema()
        return [
            cls._biographical_details_function(schema),
            cls._personal_statement_function(schema),
            cls._positions_appointments_honors_function(schema),
            cls._contributions_to_science_function(schema),
            cls._bibliography_link_function(schema)
        ]
    
    @classmethod
    def required_biographical_details_attributes(cls):
        return [
            "omb_no",
            "revision_date",
            "approved_through",
            "name",
            "era_commons_username",
            "position_title",
            "education"
        ]

    @classmethod
    def _biographical_details_function(cls, schema):
        functiondef = cls._function_for(
            "extract_biographical_details",
            "Extracts biographical information from a biographical sketch.",
            cls.required_biographical_details_attributes()
        )
        functiondef['function']['parameters']['properties'] = {f:{k:schema['properties'][f][k] for k in ["type","description","items"] if k in schema['properties'][f]} for f in cls.required_biographical_details_attributes() if f != "education"}
        container = functiondef['function']['parameters']['properties']
        cls._set_missing_type(container)
        cls._set_education(container, schema)
        return functiondef

    @classmethod
    def _set_education(cls, container, schema):
        container['education'] = cls._array_for(
            schema['properties']['education']['description'],
            {f:{k:schema['$defs']['Education']['properties'][f][k] for k in ["type","description"] if k in schema['$defs']['Education']['properties'][f]} for f in Education.required_attributes() if f != "institution"},
            Education.required_attributes()
        )
        cls._set_missing_type(container['education']['items']['properties'])
        institution_container = container['education']['items']['properties']
        cls._set_education_institution(institution_container, schema)

    @classmethod
    def _set_education_institution(cls, container, schema):
        container['institution'] = cls._object_for(
            schema['$defs']['Education']['properties']['institution']['description'],
            {f:{k:schema['$defs']['EducationalInstitution']['properties'][f][k] for k in ["type","description"] if k in schema['$defs']['EducationalInstitution']['properties'][f]} for f in EducationalInstitution.required_attributes()},
            EducationalInstitution.required_attributes()
        )
        cls._set_missing_type(container['institution']['properties'])

    @classmethod
    def _personal_statement_function(cls, schema):
        functiondef = cls._function_for(
            "extract_personal_statement",
            "Extracts the personal statement from a biographical sketch.",
             ["personal_statement"]
        )
        functiondef['function']['parameters']['properties'] = {
            "personal_statement": {},
        }
        container = functiondef['function']['parameters']['properties']
        cls._set_personal_statement(container, schema)
        return functiondef

    @classmethod
    def _set_personal_statement(cls, container, schema):
        container['personal_statement'] = cls._object_for(
            schema['properties']['personal_statement']['description'],
            {f:{k:schema['$defs']['PersonalStatement']['properties'][f][k] for k in ["type","description"] if k in schema['$defs']['PersonalStatement']['properties'][f]} for f in PersonalStatement.required_attributes() if f not in ["research_projects","manuscripts"]},
            PersonalStatement.required_attributes()
        )
        personal_statement_container = container['personal_statement']['properties']
        cls._set_missing_type(personal_statement_container)
        cls._set_research_projects(personal_statement_container, schema)
        manuscript_container = container['personal_statement']
        cls._set_manuscripts(schema, manuscript_container , 'PersonalStatement')

    @classmethod
    def _set_research_projects(cls, container, schema):
        container['research_projects'] = cls._array_for(
            schema['$defs']['PersonalStatement']['properties']['research_projects']['description'],
            {f:{k:schema['$defs']['ResearchProject']['properties'][f][k] for k in ["type","description"] if k in schema['$defs']['ResearchProject']['properties'][f]} for f in ResearchProject.required_attributes()},
            ResearchProject.required_attributes()
        )
        cls._set_missing_type(container['research_projects']['items']['properties'])

    @classmethod
    def _set_manuscripts(cls, schema, container, defname):
        container['properties']['manuscripts'] = cls._array_for(
            schema['$defs'][defname]['properties']['manuscripts']['description'],
            {f:{k:schema['$defs']['Manuscript']['properties'][f][k] for k in ["type","description"] if k in schema['$defs']['Manuscript']['properties'][f]} for f in Manuscript.required_attributes()},
            Manuscript.required_attributes()
        )
        cls._set_missing_type(container['properties']['manuscripts']['items']['properties'])
        container['properties']['manuscripts']['items']['properties']['manuscript_year']['type'] = "number"

    @classmethod
    def _positions_appointments_honors_function(cls, schema):
        required_attributes = ["positions", "scientific_appointments", "honors"]
        functiondef = cls._function_for(
            "extract_positions_appointments_honors",
            "Extracts positions, appointments, and honors from a biographical sketch.",
             required_attributes
        )
        functiondef['function']['parameters']['properties'] = {k:{} for k in required_attributes}
        container = functiondef['function']['parameters']['properties']
        cls._set_positions(container, schema)
        cls._set_scientific_appointments(container, schema)
        cls._set_honors(container, schema)
        return functiondef

    @classmethod
    def _set_positions(cls, container, schema):
        container['positions'] = cls._array_for(
            schema['properties']['positions']['description'],
            {f:{k:schema['$defs']['Position']['properties'][f][k] for k in ["type","description"] if k in schema['$defs']['Position']['properties'][f]} for f in Position.required_attributes()},
            Position.required_attributes()
        )
        cls._set_missing_type(container['positions']['items']['properties'])

    @classmethod
    def _set_scientific_appointments(cls, container, schema):
        container['scientific_appointments'] = cls._array_for(
            schema['properties']['scientific_appointments']['description'],
            {f:{k:schema['$defs']['ScientificAppointment']['properties'][f][k] for k in ["type","description"] if k in schema['$defs']['ScientificAppointment']['properties'][f]} for f in ScientificAppointment.required_attributes()},
            ScientificAppointment.required_attributes()
        )
        cls._set_missing_type(container['scientific_appointments']['items']['properties'])

    @classmethod
    def _set_honors(cls, container, schema):
        container['honors'] = cls._array_for(
            schema['properties']['honors']['description'],
            {f:{k:schema['$defs']['Honor']['properties'][f][k] for k in ["type","description"] if k in schema['$defs']['Honor']['properties'][f]} for f in Honor.required_attributes()},
            Honor.required_attributes()
        )
        cls._set_missing_type(container['honors']['items']['properties'])

    @classmethod
    def _contributions_to_science_function(cls, schema):
        functiondef = cls._function_for(
            "extract_contributions_to_science",
            "Extracts contributions to science from section C of a biographical sketch.",
             ["contributions_to_science"]
        )
        functiondef['function']['parameters']['properties'] = {
            "contributions_to_science": {},
        }
        container = functiondef['function']['parameters']['properties']
        cls._set_contributions_to_science(container, schema)
        return functiondef

    @classmethod
    def _set_contributions_to_science(cls, container, schema):
        container['contributions_to_science'] = cls._array_for(
            schema['properties']['contributions_to_science']['description'],
            {f:{k:schema['$defs']['ScientificContribution']['properties'][f][k] for k in ["type","description"] if k in schema['$defs']['ScientificContribution']['properties'][f]} for f in ScientificContribution.required_attributes() if f not in ["manuscripts"]},
            ScientificContribution.required_attributes()
        )
        cls._set_missing_type(container['contributions_to_science']['items']['properties'])
        manuscript_container = container['contributions_to_science']['items']
        cls._set_manuscripts(schema, manuscript_container , 'ScientificContribution')

    @classmethod
    def _bibliography_link_function(cls, schema):
        required_attributes = ["bibliography_link"]
        functiondef = cls._function_for(
             "extract_bibliography_link",
            "Extracts the bibliography link from a biographical sketch.",
             ["bibliography_link"]
        )
        functiondef['function']['parameters']['properties'] = {f:{k:schema['properties'][f][k] for k in ["type","description","items"] if k in schema['properties'][f]} for f in required_attributes}
        cls._set_missing_type(functiondef['function']['parameters']['properties'])
        return functiondef

  # utility methods for tools
    @classmethod
    def _function_for(cls, name, description, required_attributes):
        return {
            "type": "function",
            "function": {
                "name": name,
                "description": description,
                "parameters": {
                    "type": "object",
                    "required": required_attributes
                }
            }
        }

    @classmethod
    def _array_for(cls, description, properties, required_attributes):
        return {
            "type": "array",
            "description": description,
            "items": {
                "type": "object",
                "properties": properties,
                "required": required_attributes
            }
        }

    @classmethod
    def _object_for(cls, description, properties, required_attributes):
        return {
            "type": "object",
            "description": description,
            "properties": properties,
            "required": required_attributes
        }

    @classmethod
    def _set_missing_type(cls, property):
        for k in [k for k in property if 'type' not in property[k]]:
            property[k]['type'] = 'string'
