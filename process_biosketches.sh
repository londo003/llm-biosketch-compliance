#!/bin/bash

# Directory containing the PDF files
DIRECTORY="working/biosketches"

# Loop through each PDF file in the directory
for pdf_file in "$DIRECTORY"/*.pdf; do
	# Run the biosketch_parse.py script with the --biosketch argument
	echo "Processing file: $pdf_file"
	#python biosketch_parse.py --provider openai --model gpt-3.5-turbo-0125 --biosketch "$pdf_file" | grep '^ - '
	python biosketch_parse.py --provider azure --model gpt-4o --biosketch "$pdf_file"

done
