#!/usr/local/bin/python3
import argparse
import sys
import json
from usage_tracker import UsageTracker
from biosketch_ai import BioSketch, AIBioSketchExtractor
from biosketch_validator import BioSketchValidator

if __name__ == "__main__":
    # Argument parser setup
    parser = argparse.ArgumentParser(description='Process biosketches.')
    parser.add_argument('--provider', type=str, choices=['openai', 'azure', 'ollama'], help='Specify the AI provider to use.')
    parser.add_argument('--model', type=str, choices=['gpt-4-1106-preview','gpt-4-turbo', 'gpt-4o', 'gpt-35-turbo','llama3:8b', 'orca2:13b', 'nexusraven:13b'], help='Specify the AI model to use.')
    parser.add_argument('--biosketch', type=str, help='Path to biosketch')
    parser.add_argument('--granting_institution', type=str, choices=['NIH'], help='Specify the Granting Institution.')
    parser.add_argument('--no_usage', help='turn off usage collection with polars', action='store_true')
    args = parser.parse_args()

    # Use the parsed arguments
    biosketch_path = args.biosketch
    provider = args.provider
    model = args.model
    granting_institution = args.granting_institution
    no_usage = args.no_usage
 
    if no_usage:
        usage_tracker = None
    else:
        usage_tracker = UsageTracker(model=model)

    # Extract text and page count from PDF
    biosketch = BioSketch(
        biosketch_file=biosketch_path,
        granting_institution=granting_institution)

    tools = BioSketch.tools()
    biosketch_extractor = AIBioSketchExtractor(
        model=model,
        provider=provider,
        usage_tracker=usage_tracker
    )

    # Loop through each tool and call the OpenAI API
    biosketch_data = biosketch.model_dump()
    for tool in tools:
        data = biosketch.data_for_tool(tool)
        if data:
            biosketch_data_json = biosketch_extractor.extract(data=data, tool=tool)
            new_data = json.loads(biosketch_data_json)
            biosketch_data.update(new_data)
        else:
            print(f'Warning: could not extract {tool["function"]["name"]} data from the biosketch', file=sys.stderr)

    validator = BioSketchValidator.for_granting_institution(granting_institution)

    biosketch = biosketch.model_validate(biosketch_data)
    if usage_tracker:
        usage_tracker.save()
    validator.validate(biosketch)
    if len(biosketch.errors) > 0:
        print(f'Biosketch has {len(biosketch.errors)} errors', file=sys.stderr)
    biosketch.archive()
