from .nih_biosketch_validator import NIHBioSketchValidator

class BioSketchValidator():
    @classmethod
    def for_granting_institution(cls, type):
        if type == 'NIH':
            return NIHBioSketchValidator()
        else:
            raise ValueError(type)
