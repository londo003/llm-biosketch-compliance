# ignore verify false warnings on machines with cacert issues
import urllib3
urllib3.disable_warnings()
from datetime import datetime, timezone, timedelta
import requests
from .biosketch_validator_interface import BioSketchValidatorInterface
import re

class NIHBioSketchValidator(BioSketchValidatorInterface):
    def validate(self, biosketch):
        biosketch.validator_version = type(self).version()
        self._validate_general_compliance_checks(biosketch)
        self._validate_basic_identifying_information(biosketch)
        self._validate_education_training(biosketch)
        self._validate_personal_statement(biosketch)
        self._validate_personal_statement_products(biosketch)
        self._validate_positions_appointments_honors(biosketch)
        self._validate_contributions_to_science(biosketch)
        self._validate_biography_link(biosketch)
        return

    def _validate_general_compliance_checks(self, biosketch):
        self._validate_page_count(biosketch)
        self._validate_approved_through(biosketch)
        self._validate_urls(biosketch)
        self._validate_margins(biosketch)
        self._validate_fonts(biosketch)
        self._validate_color(biosketch)
        
    def _validate_page_count(self, biosketch):
        # Biosketch.docx General Compliance b3
        if biosketch.page_count > 5:
            biosketch.errors.append("Page count exceeds 5 pages.")

    def _validate_approved_through(self, biosketch):
        # Check 2: Form date should be in the future
        # Biosketch.docx General Compliance b8
        if biosketch.approved_through:
            if biosketch.approved_through.replace(tzinfo=timezone.utc) <= datetime.now().replace(tzinfo=timezone.utc):
                biosketch.errors.append("Form approved through date is not in the future.")
        else:
            biosketch.errors.append("Approved through date is missing or is not in the valid format.")

    def _validate_urls(self, biosketch):
        # Biosketch.docx General Compliance b1
        if biosketch.has_unacceptable_urls():
            biosketch.errors.append("Found urls to sites other than ncbi.nlm.nih.gov.")

    def _validate_margins(self, biosketch):
        # Biosketch.docx General Compliance b5
        if biosketch.left_margin < 0.5:
            biosketch.errors.append(f'Found left margin {biosketch.left_margin} less than 0.5 inches')
        if biosketch.right_margin < 0.5:
            biosketch.errors.append(f'Found right margin {biosketch.right_margin} less than 0.5 inches')

    def _validate_fonts(self, biosketch):        
        # Biosketch.docx General Compliance b6
        desired_fonts = ["Arial", "Georgia", "Helvetica", "Palatino Linotype"]
        matching_fonts = [f for f in desired_fonts if re.compile(re.escape(f), re.IGNORECASE).search(biosketch.most_common_font)]
        if len(matching_fonts) < 1:
            biosketch.errors.append('Biosketch contains unacceptable fonts.')

        # Biosketch.docx General Compliance b7
        if biosketch.most_common_size < 10.9: #fonts are more complicated then word with internal rounding and different size points
            biosketch.errors.append(f"Biosketch contains fonts smaller than 11pt")

    def _validate_color(self, biosketch):
        # Biosketch.docx General Compliance b4
        for segment_info in biosketch.colored_segments:
            biosketch.errors.append(f"Unexpected use of color in text \'{segment_info['segment']}\' with color {segment_info['color']}.")
            
    def _validate_basic_identifying_information(self, biosketch):
        # Biosketch.docx Basic Identifying Info check
        for required_field in ['era_commons_username', 'name', 'position_title']:
            if not getattr(biosketch, required_field):
                biosketch.errors.append(f'Biosketch biographical information missing {required_field} data')

    def _validate_education_training(self, biosketch):
            # Biosketch.docx Education Training data check b1
            if len(biosketch.education) > 0:
                self._validate_eduction_attributes(biosketch)
                self._validate_eduction_institution_attributes(biosketch)
                self._validate_education_dates(biosketch)
            else:
                biosketch.errors.append(f'Education institutions are missing')

    def _validate_eduction_attributes(self, biosketch):
         for i, education in enumerate(biosketch.education, start=1):
            # Biosketch.docx Education Training data check b2
            for required_field in ['degree','completion_date','field_of_study']:
                if not getattr(education, required_field):
                    biosketch.errors.append(f'Education {i} is missing {required_field} data')

    def _validate_eduction_institution_attributes(self, biosketch):
        institutions = [edu.institution for edu in biosketch.education]
        for i, institution in enumerate(institutions, start=1):
            # Check 3: Education must have name, location, degree received, mon/year completed, field of study
            # Biosketch.docx Education Training data b1, b2 check b2
            if not institution.name:
                biosketch.errors.append(f'Education institution {i} is missing name.')
            if not institution.location:
                biosketch.errors.append(f'Education institution {i} is missing location.')

    def _validate_education_dates(self, biosketch):
        # Biosketch.docx Education Training check b3
        education_dates = [edu.completion_date for edu in biosketch.education]
        if None in education_dates:
            biosketch.errors.append("One or more Eduction completion dates are missing.")
        else:
            if education_dates != sorted(education_dates):
                biosketch.errors.append("Education completion dates are not in chronological order.")

    def _validate_personal_statement(self, biosketch):
        if biosketch.personal_statement:
            personal_statement = biosketch.personal_statement
            if not personal_statement.statement:
                biosketch.errors.append('Personal Statement Narrative is missing.')
            self._validate_personal_statement_point_of_view(biosketch)
            self._validate_personal_statement_research_projects(biosketch)
        else:
            biosketch.errors.append('Personal Statement is missing.')

    def _validate_personal_statement_point_of_view(self, biosketch):
        # Biosketch.docx Personal Statement - Narrative check b4
        if biosketch.personal_statement.point_of_view:
            if biosketch.personal_statement.point_of_view != 'first':
                biosketch.errors.append('Biosketch personal statement narrative is not in the first person.')
        else:
            biosketch.errors.append('Biosketch personal statement point of view is missing')

    def _validate_personal_statement_research_projects(self, biosketch):
        personal_statement = biosketch.personal_statement
        if len(personal_statement.research_projects) > 0:
            for i, research_project in enumerate(personal_statement.research_projects, start=1):
                self._validate_research_project_fields(i, biosketch, research_project)
                self._validate_research_project_date(i, biosketch, research_project)
        else:
            biosketch.errors.append('Personal Statement Research Projects are missing.') 

    def _validate_research_project_fields(self, i, biosketch, research_project):
        # Biosketch.docx Personal Statement Narrative check b3
        for required_field in ['identifier','pi_name','project_role','start_date','end_date','title']:
            if not getattr(research_project,required_field):
                biosketch.errors.append(f'research_project {i} is missing {required_field}.')

    def _validate_research_project_date(self, i, biosketch, research_project):
        # Biosketch.docx Personal Statement Narrative check b2
        if research_project.end_date:
            time_ago = datetime.now().replace(tzinfo=timezone.utc) - research_project.end_date
            years_ago = int(time_ago.days / 365)
            if years_ago > 3:
                biosketch.errors.append(f'research project {i} is older than 3 years.')

    def _validate_personal_statement_products(self, biosketch):
        # Check 8: Count of citations for personal statement should be <= 4
        # Biosketch.docx Personal Statement - Products check b2
        if len(biosketch.personal_statement.manuscripts) > 4:
            biosketch.errors.append("Count of citations for personal statement exceeds 4.")
        self._validate_personal_statement_manuscript_pmcid(biosketch)

    def _validate_personal_statement_manuscript_pmcid(self, biosketch):
        # Check 10: Every manuscript should have an associated PMCID if date is > 2010
        for manuscript in biosketch.personal_statement.manuscripts:
            if manuscript.manuscript_year and manuscript.manuscript_year.replace(tzinfo=timezone.utc) > datetime(2010,1,1,tzinfo=timezone.utc) and not manuscript.manuscript_pmcid:
                biosketch.errors.append(f"Manuscript '{manuscript.manuscript_title}' in personal statement does not have a PMCID.")


    def _validate_positions_appointments_honors(self, biosketch):
        self._validate_appointment_dates(biosketch)
        self._validate_position_dates(biosketch)
        self._validate_honors_dates(biosketch)

    def _validate_appointment_dates(self, biosketch):
        # Check 6: Scientific appointments dates should be in reverse chronological order
        # Biosketch.docx B. Positions, Scientific Appointments and Honors check b1
        appointment_starts = [appointment.start_date for appointment in biosketch.scientific_appointments]
        if None in appointment_starts:
            biosketch.errors.append("One or more Scientific Appointment start_dates are missing.")
        else:
            if len(appointment_starts) > 0:
                if appointment_starts != sorted(appointment_starts, reverse=True):
                    biosketch.errors.append("Scientific appointments dates are not in reverse chronological order.")
            else:
                biosketch.errors.append("Scientific appointment dates are missing.")
        for i, appointment in enumerate(biosketch.scientific_appointments, start=1):
            self._validate_appointment_current(biosketch, i, appointment)

    def _validate_appointment_current(self, biosketch, i, appointment):
        # Biosketch.docx check b2 appointments should be current
        if appointment.end_date:
            biosketch.errors.append(f'Scientific Appointment {i} ending on {appointment.end_date} is not current.')

    def _validate_position_dates(self, biosketch):
        # Check 4: Positions dates should be in reverse chronological order
        # Biosketch.docx B. Positions, Scientific Appointments and Honors check b1
        position_starts = [pos.start_date for pos in biosketch.positions]
        if None in position_starts:
            biosketch.errors.append('One or more Position start_dates dates are missing.')
        else:
            if position_starts != sorted(position_starts, reverse=True):
                biosketch.errors.append("Positions dates are not in reverse chronological order.")
        for i, position in enumerate(biosketch.positions, start=1):
            self._validate_position_current(biosketch, i, position)

    def _validate_position_current(self, biosketch, i, position):
        # Biosketch.docx check b2 positions should be current
        if position.end_date:
            biosketch.errors.append(f'Position {i} ending on {position.end_date} is not current.')

    def _validate_honors_dates(self, biosketch):
        # Check 5: Honors dates should be in reverse chronological order
        # Biosketch.docx B. Positions, Scientific Appointments and Honors check b1
        honor_dates = [honor.start_date for honor in biosketch.honors]
        if None in honor_dates:
            biosketch.errors.append("One or ore Honors start dates are missing.")
        else:
            if honor_dates != sorted(honor_dates, reverse=True):
                biosketch.errors.append("Honors dates are not in reverse chronological order.")

    def _validate_contributions_to_science(self, biosketch):
        # Check 7: Count of scientific contributions should be <= 5
        # Biosketch.docx C. Contributions to Science check b1
        if len(biosketch.contributions_to_science) > 5:
            biosketch.errors.append("Count of scientific contributions exceeds 5.")
        self._validate_contribution_citations(biosketch)
        self._validate_contribution_manuscript_pmcid(biosketch)

    def _validate_contribution_citations(self, biosketch):
        # Check 9: Count of citations for each contribution to science should be <= 4
        # Biosketch.docx C. Contributions to Science check b1
        for i, contribution in enumerate(biosketch.contributions_to_science, start=1):
            if len(contribution.manuscripts) > 4:
                biosketch.errors.append(f"Count of citations for contribution {i} exceeds 4.")

    def _validate_contribution_manuscript_pmcid(self, biosketch):
        for i, contribution in enumerate(biosketch.contributions_to_science, start=1):
            for manuscript in contribution.manuscripts:
                if manuscript.manuscript_year and manuscript.manuscript_year.replace(tzinfo=timezone.utc) > datetime(2010,1,1, tzinfo=timezone.utc) and not manuscript.manuscript_pmcid:
                    biosketch.errors.append(f"Manuscript '{manuscript.manuscript_title}' in contribution {i} does not have a PMCID.")

    def _validate_biography_link(self, biosketch):
        # Check 11: Check bibliography link
        if biosketch.bibliography_link:
            try:
                response = requests.get(biosketch.bibliography_link, verify=False)
                if not response.status_code == 200:
                    biosketch.errors.append("Error accessing bibliography link.")
            except requests.exceptions.RequestException as e:
                biosketch.errors.append("Error accessing bibliography link.")
        else:
            biosketch.errors.append("Bibliography link is missing.")

    @classmethod
    def version(cls):
        return 'v1.0.2'