import abc

class BioSketchValidatorInterface(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'validate') and 
                callable(subclass.validate) or
                NotImplemented)

    @abc.abstractmethod
    def validate(biosketch):
        """Validate the Biosketch"""
        raise NotImplementedError

    @classmethod
    def version(cls):
        """Each implementation must have a version"""
        raise NotImplementedError
