import polars as pl
from datetime import datetime
import uuid

class UsageTracker:
    def __init__(self, model):
        self.run_id = str(uuid.uuid4())
        self.model = model
        self.usage = {
            'run_id': [],
            'call_date': [],
            'model': [],
            'function': [],
            'prompt_tokens': [],
            'completion_tokens': []
        }

    @classmethod
    def version(cls):
        return 'v1.0.1'
    
    @classmethod
    def usage_dir(cls):
        return f'usage/{cls.version()}'

    def track(self, function, prompt_tokens, completion_tokens):
        self.usage['run_id'].append(self.run_id)
        self.usage['model'].append(self.model)
        self.usage['call_date'].append(datetime.now().isoformat())
        self.usage['prompt_tokens'].append(prompt_tokens)
        self.usage['function'].append(function)
        self.usage['completion_tokens'].append(completion_tokens)

    def save(self):
        """
        Takes a usage dict, turns it into a Polars DataFrame, and writes it to usage/{datetime}.parquest
        If usage is None, does not write the file
        Args:
            usage (Dict): Hash with colum keys and array values collecting data about each call to openai
        """
        df = pl.DataFrame(self.usage)
        usage_file = f'{self.__class__.usage_dir()}/{datetime.now().strftime("%Y_%b_%d_%H_%M_%S")}.parquet'
        df.write_parquet(usage_file)
